import { h, Component } from 'preact';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone, faTimes } from '@fortawesome/free-solid-svg-icons';
import { CSSTransition } from 'react-transition-group';

import style from './callpopup.css';


class CallPopup extends Component {
  render() {
    let popup;
    switch (this.props.type) {
      case "calling":
        popup = (
          <div className={style.popup}>
            <FontAwesomeIcon icon={faPhone} style={{width: "3rem", height: "3rem"}} className="mb-5"/>
            <h3 className="text-2xl">Calling...</h3>
            <p className="text-lg">Hang on...we're connecting you.</p>
          </div>
        );
        break;
      case "hosting":
        popup = (
          <div className={style.popup}>
            <FontAwesomeIcon icon={faPhone} style={{width: "3rem", height: "3rem"}} className="mb-5"/>
            <h3 className="text-2xl">Meeting open!</h3>
            <p classname="text-lg">Send this ID to your participants:</p>
            <p className="text-lg">{this.props.peerID}</p>
            <br />
            <button className="rounded-lg shadow-lg bg-green-400 text-white p-3 w-full" onClick={this.props.onAccept}>
              OK
            </button>
          </div>
        );
        break;
      default:
        popup = (
          <div className={style.popup}>
            <FontAwesomeIcon icon={faPhone} style={{width: "3rem", height: "3rem"}} className="mb-5"/>
            <h3 className="text-2xl">Calling...</h3>
            <p className="text-lg">Hang on...we're connecting you.</p>
          </div>
        );
    }

    return (
      <CSSTransition
        in={this.props.in}
        timeout={300}
        classNames="callpopup"
        unmountOnExit
      >
        {popup}
      </CSSTransition>
    );
  }
}

export default CallPopup;
