import { h, Component } from 'preact';
import { Router } from 'preact-router';
import Peer from 'peerjs';

// Code-splitting is automated for routes
import Call from '../routes/call';
import Home from '../routes/home';
import NotFoundPage from '../routes/404';
import PeerNotFoundPage from '../routes/peer-not-found';

export default class App extends Component {
  state = {
    peerID: ""
  }

  constructor(props) {
    super(props);
    this.peer = new Peer({
      host: '10.0.0.227',
      port: 9000,
      path: '/'
    });
    this.peer.on('open', () => {
      this.setState({ peerID: this.peer.id });
      console.log(this.peer.id);
    });
  }

	/** Gets fired when the route changes.
	 *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
	 *	@param {string} event.url	The newly routed URL
	 */
	handleRoute = e => {
		this.currentUrl = e.url;
	};

	render() {
		return (
			<div id="app">
				<Router onChange={this.handleRoute}>
          <Call peerID={this.state.peerID} peer={this.peer} path="/call/:connectedID" />
          <Home peerID={this.state.peerID} peer={this.peer} path="/" />
          <PeerNotFoundPage path="/peer-not-found" />
          <NotFoundPage default />
				</Router>
			</div>
		);
	}
}
