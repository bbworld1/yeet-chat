import { h, Component } from 'preact';

class RoundButton extends Component {
  render() {
    return (
      <button className={`mx-4 rounded-full flex justify-center items-center shadow-lg transition-all duration-200 ${this.props.bgColor} ${this.props.fgColor}`} style={{width: "3.25rem", height: "3.25rem"}} {...this.props}>
        {this.props.children}
      </button>
    );
  }
}

export default RoundButton;
