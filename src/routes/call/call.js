import { h, Component, createRef } from 'preact';
import { route } from 'preact-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faPhone,
  faUsers,
  faCommentAlt,
  faMicrophone,
  faVideo,
  faMicrophoneSlash,
  faVideoSlash
} from '@fortawesome/free-solid-svg-icons';
import Peer from 'peerjs';
import CallPopup from '../../components/CallPopup';
import RoundButton from '../../components/RoundButton';

class Call extends Component {
  mainVideoRef = createRef();
  peer = null;
  call = null;

  state = {
    connectedID: '',
    showHostPopup: true,
    showCallPopup: true,
    audio: false,
    video: false
  }

  componentDidMount() {
    this.peer = this.props.peer;
    this.peer.on('connection', (conn) => {
      conn.on('open', () => {
        console.log("Connection Established!");
        this.peer.on('data', (data) => {
          console.log(data);
        });
      });
    });
    this.peer.on('call', (call) => {
      console.log("Receiving call...");
      this.call = call;
      let getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
      getUserMedia({ video: true, audio: true }, (stream) => {
        this.call.answer(stream);
        this.call.on('stream', (remoteStream) => {
          // Show stream in some video/canvas element.
          this.mainVideoRef.current.srcObject = remoteStream
        });
      });
    });
    this.setState({ peerID: this.props.peerID });

    if (this.props.connectedID === 'host-meeting') {
      this.setState({ showHostPopup: true });
    } else {
      let getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
      getUserMedia({ video: true, audio: true }, (stream) => {
        this.setState({ showCallPopup: true });
        this.call = this.peer.call(this.props.connectedID, stream);
        this.call.on('stream', (remoteStream) => {
          this.setState({ showCallPopup: false });
          this.mainVideoRef.current.srcObject = remoteStream;
          console.log(localStream);
        });
      }, (err) => console.error("Couldn't get stream", err));
    }
  }

  toggleAudio = () => {
    this.setState({ audio: !this.state.audio });
  }

  toggleVideo = () => {
    this.setState({ video: !this.state.video });
  }

  endCall = () => {
    if (this.call) this.call.close();
    route('/');
  }

  closeHostPopup = () => {
    this.setState({ showHostPopup: false });
  }

  render() {
    return (
      <main>
        <div className="text-white py-2 px-4 rounded-md bg-opacity-50 bg-black fixed" style={{left: "5rem", top: "2rem"}}>
          <p>Yeetmaster 69</p>
          { this.props.connectedID === "host-meeting" && <p>Peer ID: {this.props.peerID}</p> }
        </div>
        { this.props.connectedID === 'host-meeting' ?
          <CallPopup type="hosting" in={this.state.showHostPopup} peerID={this.props.peerID} onAccept={this.closeHostPopup} /> :
          <CallPopup type="calling" in={this.state.showCallPopup} />
        }
        <video style={{width: "100vw", height: "100vh", backgroundColor: "black"}} autoPlay ref={this.mainVideoRef}></video>
        <div className="absolute flex justify-center" style={{bottom: "4vh", width: "100vw"}}>
          <RoundButton onClick={this.toggleVideo} bgColor={this.state.video ? "bg-green-500" : "bg-orange-600"} fgColor="text-white">
            { this.state.video ?
              <FontAwesomeIcon icon={faVideo} /> :
              <FontAwesomeIcon icon={faVideoSlash} />
            }
          </RoundButton>
          <RoundButton onClick={this.toggleAudio} bgColor={this.state.audio ? "bg-green-500" : "bg-orange-600"} fgColor="text-white">
            { this.state.audio ?
              <FontAwesomeIcon icon={faMicrophone} /> :
              <FontAwesomeIcon icon={faMicrophoneSlash} />
            }
          </RoundButton>
          <RoundButton bgColor="bg-gray-300" fgColor="text-gray-700">
            <FontAwesomeIcon icon={faUsers} />
          </RoundButton>
          <RoundButton bgColor="bg-gray-300" fgColor="text-green-600">
            <FontAwesomeIcon icon={faCommentAlt} />
          </RoundButton>
          <RoundButton onClick={this.endCall} bgColor="bg-red-600" fgColor="text-white">
            <FontAwesomeIcon icon={faPhone} />
          </RoundButton>
        </div>
      </main>
    );
  }
}

export default Call;
