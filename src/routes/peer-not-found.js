import { h, Component } from 'preact';

class PeerNotFoundPage extends Component {
  render() {
    return (
      <div className="flex flex-col justify-center text-center" style={{width: "100vw", minHeight: "100vh"}}>
        <h1 className="text-5xl">Peer ID Not Found</h1>
        <p className="text-lg my-3">Looks like the Peer ID entered was invalid!</p>
        <p className="text-lg my-3">Please check to make sure your Peer ID or link is valid.</p>
        <a className="text-teal-600" href="/">Home</a>
      </div>
    );
  }
}

export default PeerNotFoundPage;
