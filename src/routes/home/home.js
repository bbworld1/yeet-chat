import { h, Component } from 'preact';
import { route } from 'preact-router';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

import style from './home.module.css';

class Home extends Component {
  state = {
    peerID: '',
    showCallPopup: false,
    callStatus: ''
  }

  constructor(props) {
    super(props);
  }

  onIDChange = (e) => {
    this.setState({ peerID: e.target.value });
  }

  onKeyUp = (e) => {
    if (this.state.peerID && e.keyCode === 13) route('/call/' + this.state.peerID);
  }

  onConnect = () => {
    if (this.state.peerID) route('/call/' + this.state.peerID);
  }

  onHostMeeting = () => {
    route('/call/host-meeting');
  }

  render() {
    return (
      <main className="flex flex-col justify-center items-center text-center" style={{width: "100vw", height: "100vh"}}>
        <h2 className="text-6xl"><span className="text-orange-600">Y</span>eet<span className="text-orange-600">C</span>hat</h2>
        <p className="text-xl my-3 font-light">A simple, secure P2P video chat.</p>
        <p className="text-lg font-light">Enter a Peer ID below:</p>
        <br />
        <div>
          <input
            className={style.homeinput}
            value={this.state.peerID}
            onChange={this.onIDChange}
            onKeyUp={this.onKeyUp}
            placeholder="Enter a Peer ID..."
          />
          <button
            className={style.homebtn + " " + style.squarebtn}
            onClick={this.onConnect}
          >
            <FontAwesomeIcon icon={faSearch} />
          </button>
        </div>
        <p className="my-3 text-lg font-light">or</p>
        <button className={style.homebtn} style={{minWidth: "20vw"}} onClick={this.onHostMeeting}>
          Host a Meeting
        </button>
      </main>
    );
  }
}

export default Home;
