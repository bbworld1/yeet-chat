import { h, Component } from 'preact';

class NotFoundPage extends Component {
  render() {
    return (
      <div className="flex flex-col justify-center text-center" style={{width: "100vw", minHeight: "100vh"}}>
        <h1 className="text-5xl">404: Page Not Found</h1>
        <p className="text-lg my-3">Whoops - looks like you stumbled upon this page by accident.</p>
        <a className="text-teal-600" href="/">Let's go back to safety.</a>
      </div>
    );
  }
}

export default NotFoundPage;
