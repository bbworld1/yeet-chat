# YeetChat: a FOSS, distributed P2P video chat solution.
YeetChat (Yet Another E-Chat Transmitter - yes, we know that spells Yaet, but Yeet looks better)
is a free and open source peer-to-peer video chat solution.
Based on WebRTC and PeerJS, it provides easy-to-use, secure video
chat that doesn't involve a server and is accessible from anywhere,
including your web browser.

Note that YeetChat is still in development and as such is currently not complete.

## How does YeetChat work?
YeetChat is based on the WebRTC real-time communication standard
implemented by almost all modern browsers. Because of this, we're
able to cut the technical bloat of a proprietary video chat protocol
and focus on delivering a secure solution that feels great to use.

In order to facilitate peer-to-peer WebRTC communication, we use the
PeerJS library to connect clients to each other. This allows us to
bypass having data go through servers, resulting in a solution that's
both secure and cheaper to host.

### Wait - PeerJS needs a server!
You're correct - PeerJS (and by extension, YeetChat) does use a server.
However, this server only functions as a *connection broker* - that is,
it connects clients to each other. Once clients are connected to each
other, data passes directly between the clients - no data passes through
servers.

Note that depending on your network configuration, you might need to have
your traffic relayed through a TURN server in order to be accessible. This
means your data *may* have to go through an external server. Don't worry though -
your data is still end-to-end encrypted, so everything is still secure! (Of course,
if you *really* want security, you can always set up your own TURN server - more on
that below.)

## Cool! How do I use it? ##
You can connect to our publicly accessible YeetChat web client
(once we get it set up). We use the free cloud-hosted version of PeerServer
as our connection broker.

If you'd like to host your own YeetChat frontend and connection broker,
here's how to do it:
1. [Set up a PeerServer instance.](https://github.com/peers/peerjs-server)
2. Clone this repository.
3. Change `{ customPeerServer: { host: yourHost, port: yourPort, path: yourPath }}` in `config.json`.
4. `npm install`
5. `npm run build`
6. Serve the `build` folder with an HTTP server of your choice.


## CLI Commands

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# test the production build locally
npm run serve

# run tests with jest and preact-render-spy
npm run test
```

For detailed explanation on how things work, checkout the [CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).
